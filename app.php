<?php

include('vendor/autoload.php');

use AppTerm\SimpleCommand;

$app = new AppTerm\App();
$app->setDebugMode(false);

$simpleCommand = new SimpleCommand(
    'test',
    'Just test command',
    false,
    false,
    function ($ar, $op) {
        echo sprintf("Called command: %s\n", $this->getName());

        echo "Arguments:\n";
        foreach ($ar as $argument){
            echo sprintf("  - %s\n", $argument);
        }

        echo "Options:\n";
        foreach ($op as $optionName => $optionValues){
            echo sprintf("  - %s:\n", $optionName);
            foreach ($optionValues as $optionValue){
                echo sprintf("    - %s\n", $optionValue);
            }
        }
    }
);

$app->addCommand($simpleCommand);

$app->run();