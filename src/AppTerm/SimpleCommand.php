<?php


namespace AppTerm;

/**
 * Class SimpleCommand
 * @package AppTerm
 */
class SimpleCommand extends Command
{
    /**
     * @var array
     */
    protected $arguments;
    /**
     * @var array
     */
    protected $options;

    /**
     * @var string
     */
    protected $description;

    /**
     * SimpleCommand constructor.
     * @param string $name
     * @param string $description
     * @param bool|array $arguments
     * @param bool|array $options
     * @param callable $callback
     */
    public function __construct(string $name, string $description, $arguments, $options, callable $callback)
    {
        parent::__construct($name, $callback);
        $this->description = $description;
        $this->arguments = $arguments;
        $this->options = $options;
    }

    /**
     * @param array $arguments
     * @param array $options
     */
    public function execute(array $arguments, array $options): void
    {
        if (current($arguments) === 'help'){
            $this->printHelp();
            return;
        }

        $resultArguments = [];
        $resultOptions = [];

        foreach ($arguments as $argument){
            if ($this->arguments === false || in_array($argument, $this->arguments, false)){
                $resultArguments[] = $argument;
            }
        }

        foreach ($options as $optionName => $optionValues){
            if ($this->options === false || in_array($optionName, $this->options, false)){
                $resultOptions[$optionName] = $optionValues;
            }
        }

        parent::execute($resultArguments, $resultOptions);
    }


    public function printHelp(): void
    {
        echo sprintf("Command: %s\n", $this->name);
        echo sprintf("Description:\n%s\n", $this->description);
        echo "Valid arguments:\n" . implode(', ', $this->arguments) . "\n";
        echo "Valid options:\n" . implode(', ', $this->options) . "\n";
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }
}