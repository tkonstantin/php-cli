<?php


namespace AppTerm;

/**
 * Class CommandResolver
 * @package AppTerm
 */
class CommandResolver
{
    /**
     * @param Command[] $commands
     * @param array $argv
     * @return Command
     * @throws NotEnoughArgumentsException
     * @throws CommandNotFoundException
     */
    public function matchCommand(array $commands, array $argv): CommandInterface
    {
        if (count($argv) < 2){
            throw new NotEnoughArgumentsException();
        }

        $commandName = $argv[1];

        foreach ($commands as $command){
            if ($command->getName() === $commandName){
                return $command;
            }
        }

        throw new CommandNotFoundException();
    }
}