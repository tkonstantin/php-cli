<?php


namespace AppTerm;

/**
 * Class NotEnoughArgumentsException
 * @package AppTerm
 */
class NotEnoughArgumentsException extends \Exception
{
    protected $message = 'Not enough arguments. Provide at least command name';
}