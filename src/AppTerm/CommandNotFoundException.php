<?php


namespace AppTerm;

/**
 * Class CommandNotFoundException
 * @package AppTerm
 */
class CommandNotFoundException extends \Exception
{
    protected $message = 'Command not found';
}