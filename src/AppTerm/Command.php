<?php


namespace AppTerm;

use Closure;

/**
 * Class Command
 * @package AppTerm
 */
abstract class Command implements CommandInterface
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var Closure
     */
    protected $callback;

    /**
     * Command constructor.
     * @param string $name
     * @param callable $callback
     */
    public function __construct(string $name, callable $callback)
    {
        $this->name = $name;
        $this->callback = $callback;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param array $arguments
     * @param array $options
     * @return void
     */
    public function execute(array $arguments, array $options): void
    {
        $this->callback->call($this, $arguments, $options, $this);
    }
}