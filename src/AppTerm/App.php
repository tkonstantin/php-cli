<?php

namespace AppTerm;

/**
 * Class App
 * @package AppTerm
 */
class App
{
    /**
     * @var CommandInterface[]
     */
    protected $commands = [];

    /**
     * @var bool
     */
    protected $debugMode = false;

    /**
     * @param array|null $argv
     */
    public function run(array $argv = null): void
    {
        if ($argv === null){
            global $argv;
        }

        try {
            $this->runUnstable($argv);
        } catch (\Exception $exception) {
            $this->printError($exception);
        }
    }

    /**
     * @param array $argv
     */
    public function runUnstable(array $argv): void
    {
        $commandResolver = new CommandResolver();

        try {
            $command = $commandResolver->matchCommand($this->commands, $argv);
        } catch (\Exception $exception){
            $this->debugAllCommands();
            return;
        }

        $argumentsResolver = new ArgumentsResolver();
        $arguments = $argumentsResolver->extractArguments($argv);

        $optionsResolver = new OptionsResolver();
        $options = $optionsResolver->extractOptions($argv);

        if ($this->debugMode){
            $this->debugCommand($command->getName(), $arguments, $options);
        }

        $command->execute($arguments, $options);
    }

    /**
     * @param CommandInterface $command
     */
    public function addCommand(CommandInterface $command): void
    {
        $this->commands[] = $command;
    }

    /**
     * @param \Exception $exception
     */
    protected function printError(\Exception $exception): void
    {
        echo sprintf("Error: %s\n", $exception->getMessage());
    }

    /**
     * @param bool $status
     */
    public function setDebugMode(bool $status): void
    {
        $this->debugMode = $status;
    }

    /**
     * @param string $commandName
     * @param array $arguments
     * @param array $options
     */
    protected function debugCommand(string $commandName, array $arguments, array $options): void
    {
        echo sprintf("Called command: %s\n", $commandName);

        echo "Arguments:\n";
        foreach ($arguments as $argument){
            echo sprintf("  - %s\n", $argument);
        }

        echo "Options:\n";
        foreach ($options as $optionName => $optionValues){
            echo sprintf("  - %s:\n", $optionName);
            foreach ($optionValues as $optionValue){
                echo sprintf("    - %s\n", $optionValue);
            }
        }
    }

    protected function debugAllCommands(): void
    {
        echo "Available commands:\n";
        foreach ($this->commands as $index => $command){
            echo sprintf("%d) %s: %s\n", $index + 1, $command->getName(), $command->getDescription()) ;
        }
    }
}