<?php


namespace AppTerm;

/**
 * Class ArgumentsResolver
 * @package AppTerm
 */
class ArgumentsResolver
{
    /**
     * @param array $argv
     * @return array
     */
    public function extractArguments(array $argv): array
    {

        $resolvedArguments = [];

        for ($i = 2, $argc = count($argv); $i < $argc; $i++){
            /**
             * Parsing arguments like {1} {a} {1,3} {b,cd}
             */
            preg_match('/{(.+)}/', $argv[$i], $argumentMatch);
            if ($argumentMatch && ($argument = $argumentMatch[1])){
                foreach (explode(',', $argument) as $argumentValue){
                    $resolvedArguments[] = $argumentValue;
                }
            }

            /**
             * Parsing arguments like 1 a 13 abc
             */
            preg_match('/^([^\[{](.*[^}\]])?)$/', $argv[$i], $argumentMatch);
            if ($argumentMatch && ($argumentValue = $argumentMatch[1])){
                $resolvedArguments[] = $argumentValue;
            }
        }

        return $resolvedArguments;
    }
}