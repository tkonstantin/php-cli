<?php


namespace AppTerm;

/**
 * Class OptionsResolver
 * @package AppTerm
 */
class OptionsResolver
{
    /**
     * @param array $argv
     * @return array
     */
    public function extractOptions(array $argv): array
    {

        $resolvedOptions = [];

        for ($i = 2, $argc = count($argv); $i < $argc; $i++){
            /**
             * Parsing options like [a=12] [abc=34] [bc={3,4,6,hg5}]
             */

            preg_match('/\[([^=]+)=(.*)\]/', $argv[$i], $optionMatch);
            if ($optionMatch && ($optionName = $optionMatch[1]) && ($optionValue = $optionMatch[2])){
                foreach (explode(',', $optionValue) as $optionSingleValue){
                    $resolvedOptions[$optionName][] = $optionSingleValue;
                }
            }
        }

        return $resolvedOptions;
    }
}